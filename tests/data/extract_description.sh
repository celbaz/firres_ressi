#!/bin/bash
#   This file is part of Firres.

#   Firres is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   Firres is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with Firres.  If not, see <https://www.gnu.org/licenses/>.

# $1 = path to yearly CVE file from NIST
jq -r '.CVE_Items [] | .cve.CVE_data_meta.ID, .cve.description.description_data[].value' $1