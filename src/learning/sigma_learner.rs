/**
    This file is part of Firres.

    Copyright (C) 2019  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use serde_json;
use std::collections::BTreeMap;
use std::fs::remove_file;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;
use std::path::PathBuf;

pub struct SigmaLearner {
    training_data: TrainingData,
    cache_filename: PathBuf,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct TrainingData {
    // String = CVE ID, bool = is the CVE correctly matched with its CPE entry ?
    samples: BTreeMap<String, bool>,
}

impl SigmaLearner {
    pub fn new(cache_filename: PathBuf) -> SigmaLearner {
        println!("Reconstructing learner. This might take a while...");

        let f = File::open(&cache_filename);
        let training_data = match f {
            Err(_e) => TrainingData {
                samples: BTreeMap::new(),
            },
            Ok(file) => serde_json::from_reader(BufReader::new(file)).unwrap(),
        };

        println!("Sigma learner reconstructed.");

        let f = BufWriter::new(File::create(&cache_filename).unwrap());
        serde_json::to_writer(f, &training_data).unwrap();

        SigmaLearner {
            training_data,
            cache_filename,
        }
    }

    pub fn add_sample(&mut self, cve_id: String, correct: bool) {
        self.training_data.samples.insert(cve_id, correct);

        // Remove out of date file and recreate up to date file. This is not production-ready (but the whole codebase isn't, for that matter).
        remove_file(&self.cache_filename).unwrap();
        let f = BufWriter::new(File::create(&self.cache_filename).unwrap());
        serde_json::to_writer(f, &self.training_data).unwrap();
    }
}
