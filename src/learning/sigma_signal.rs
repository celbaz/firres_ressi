/**
    This file is part of Firres.

    Copyright (C) 2019  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use learning::montecarlo;
use learning::sigma_learner::SigmaLearner;
use std::io;
use std::path::PathBuf;
use vulnerability::cpe::CPEDictionary;
use vulnerability::cve::CVEDictionary;

pub fn learn_sigma_for_signal(
    cve_dictionary: &CVEDictionary,
    cpe_dictionary: &CPEDictionary,
) -> f64 {
    let mut sigma_learner = SigmaLearner::new(PathBuf::from("cache/sigma.learning.json"));

    loop {
        let entry_index = montecarlo::select_random_entry_index(cve_dictionary);

        let is_correct = active_learning(cve_dictionary, cpe_dictionary, entry_index);

        sigma_learner.add_sample(
            cve_dictionary.entries[entry_index].cve_id.clone(),
            is_correct,
        );
    }
}

fn active_learning(
    cve_dictionary: &CVEDictionary,
    cpe_dictionary: &CPEDictionary,
    entry_index: usize,
) -> bool {
    let mut cpe_scores = Vec::new();

    let vulnerability = &cve_dictionary.entries[entry_index];

    for i in vulnerability.features.sorted_keys() {
        let score = vulnerability.features.get_feature_value(*i);
        cpe_scores.push((
            score,
            cpe_dictionary.entries[*i as usize].description.clone(),
        ));
    }

    cpe_scores.sort_by(|a, b| b.0.partial_cmp(&a.0).unwrap());

    println!("============");
    println!("{}", vulnerability.cve_id);
    println!("{}", vulnerability.description);
    println!("3 most probable CPE associated with it :");
    println!("=> {}", cpe_scores[0].1);
    println!("=> {}", cpe_scores[1].1);
    println!("=> {}", cpe_scores[2].1);
    println!("============");
    println!("Is this correct ? (y/n)");

    loop {
        let mut answer_as_string = String::new();
        io::stdin().read_line(&mut answer_as_string).unwrap();

        match answer_as_string.trim().as_ref() {
            "y" => return true,
            "n" => return false,
            _ => println!("Please answer the question ! (y/n)"),
        }
    }
}
