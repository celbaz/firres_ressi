/**
    This file is part of Firres.

    Copyright (C) 2019  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use config_crate;

#[derive(Debug, Deserialize)]
pub struct FirresConfig {
    pub cpe: CPEConfig,
    pub cve: CVEConfig,
    pub integrity: IntegrityConfig,

    pub threads: usize,
}

#[derive(Debug, Deserialize)]
pub struct IntegrityConfig {
    pub check_cve_dictionary_determinism: bool,
    pub acceptable_error_rate: f64,
    pub confidence_interval: f64,
}

#[derive(Debug, Deserialize)]
pub struct CPEConfig {
    pub repository: String,
}

#[derive(Debug, Deserialize)]
pub struct CVEConfig {
    pub repository: String,
    pub cpe_per_vul: usize,
}

impl FirresConfig {
    pub fn new() -> FirresConfig {
        let mut config = config_crate::Config::default();
        config
            .merge(config_crate::File::with_name("firres"))
            .unwrap();

        config.try_into().unwrap()
    }
}
