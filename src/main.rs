/**
    This file is part of Firres.

    Copyright (C) 2019  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
extern crate crossbeam;
extern crate rand;
extern crate xml;
#[macro_use]
extern crate lazy_static;
extern crate regex;

#[macro_use]
extern crate serde_derive;

extern crate bincode;
extern crate config as config_crate;
extern crate edit_distance;
extern crate serde;
extern crate serde_json;

mod config;
use config::FirresConfig;

mod learning;

mod vulnerability;

use learning::index::WordIndex;

mod cleanutils;

mod math;

fn main() {
    let firres_config = FirresConfig::new();

    let mut word_index = WordIndex::new();

    let cpe_dictionary =
        vulnerability::cpe::build_cpe_dictionary(firres_config.cpe, &mut word_index);

    println!(
        "CPE Dictionary import complete. {} entries were added",
        cpe_dictionary.entries.len()
    );

    let cve_dictionary =
        vulnerability::cve::build_cve_dictionary(&cpe_dictionary, &firres_config.cve);

    println!(
        "CVE Dictionary import complete. {} entries were added",
        cve_dictionary.entries.len()
    );

    if firres_config.integrity.check_cve_dictionary_determinism {
        println!("Recomputing the CVE dictionary from scratch a second time to assert that its generation is deterministic");

        let cve_dictionary2 =
            vulnerability::cve::build_cve_dictionary(&cpe_dictionary, &firres_config.cve);

        assert_eq!(cve_dictionary.entries.len(), cve_dictionary2.entries.len());

        for i in 0..cve_dictionary.entries.len() {
            let first = &cve_dictionary.entries[i];
            let second = &cve_dictionary2.entries[i];

            assert_eq!(first.cve_id, second.cve_id);
            assert_eq!(first.description, second.description);
            assert_eq!(first.words, second.words);
            assert_eq!(first.features, second.features);
        }
    }

    let mut without_keyword_count = 0;
    for entry in &cve_dictionary.entries {
        let mut has_no_match = entry.features.sorted_keys().is_empty();
        if has_no_match {
            without_keyword_count += 1;
            println!("=================");
            println!(
                "Vulnerability {} did not match with any keyword.",
                entry.cve_id
            );
            println!("{}", entry.description);
            println!("=================");
        }
    }
    println!(
        "There are {} vulnerabilities out of {} that did not match with any keyword.",
        without_keyword_count,
        cve_dictionary.entries.len()
    );

    learning::sigma_signal::learn_sigma_for_signal(&cve_dictionary, &cpe_dictionary);
}
